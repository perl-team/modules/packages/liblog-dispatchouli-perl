Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Log-Dispatchouli
Upstream-Contact: Ricardo SIGNES <cpan@semiotic.systems>
Source: https://metacpan.org/release/Log-Dispatchouli

Files: *
Copyright: 2025, Ricardo SIGNES <cpan@semiotic.systems>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2010-2025, gregor herrmann <gregoa@debian.org>
 2010-2011, Jonathan Yu <jawnsy@cpan.org>
 2011, Jotam Jr. Trejo <jotamjr@debian.org.sv>
 2013-2021, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
